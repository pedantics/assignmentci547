<?php

require_once __DIR__ . '/vendor/autoload.php';

use Pecee\SimpleRouter\SimpleRouter;

require_once 'vendor/pecee/simple-router/helpers.php';
require_once 'src/routes.php';

SimpleRouter::setDefaultNamespace("\CI527\controllers");

SimpleRouter::start();