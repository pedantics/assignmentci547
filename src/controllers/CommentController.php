<?php


namespace CI527\controllers;


use CI527\models\Comment;
use CI527\services\CommentService;

class CommentController
{

    private $commentService;

    public function __construct()
    {
        $this->commentService = new CommentService();
    }

    public function getComments()
    {
        response()->header('Content-type: application/json; charset=UTF-8');
        response()->httpCode(200);
        return json_encode($this->commentService->getAllComments(), JSON_PRETTY_PRINT);
    }

    public function getCommentsForObject(string $oid) {
        response()->header('Content-type: application/json; charset=UTF-8');
        $output = $this->commentService->getCommentsForObject($oid);

        if ($output["comments"] == null) {
            response()->header("NO CONTENT");
            response()->httpCode(204);
            return "";
        }

        return json_encode($output, JSON_PRETTY_PRINT);
    }

    public function createComment(Comment $comment) {
        response()->header('Content-type: application/json; charset=UTF-8');
        $commentId = $this->commentService->createComment($comment);

        if ($commentId > 0)
        {
            response()->httpCode(201);
            return json_encode($commentId);
        }

        response()->httpCode(400);
    }
}