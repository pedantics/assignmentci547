<?php


namespace CI527\controllers;


class HelloWorldController
{
    public function hello($name = "World") {
        echo "Hello " . $name;
    }
}