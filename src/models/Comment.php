<?php


namespace CI527\models;


class Comment
{

    public string $oid;
    public int $id;
    public string $comment;
    public string $name;
    public string $updatedAt;
    public string $createdAt;

    public static function newInstance()
    {
        return new Comment();
    }

    public function oid(string $oid): Comment
    {
        $this->oid = $oid;
        return $this;
    }

    public function comment(string $comment): Comment
    {
        $this->comment = $comment;
        return $this;
    }

    public function author(string $author): Comment
    {
        $this->name = $author;
        return $this;
    }

    public function updatedAt(string $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function createdAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }


}