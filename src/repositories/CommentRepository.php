<?php


namespace CI527\repositories;

use CI527\models\Comment;
use \PDO as PDO;

class CommentRepository
{
    private $conn;

    public function __construct()
    {
        $this->conn = new PDO('mysql:host=localhost;dbname=test', 'root', '');
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function fetchComments(): array
    {
        $stmt = $this->conn->query("SELECT object_id, author, comment_text FROM comments");
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function createComment(Comment $comment): int
    {
        $stmt = $this->conn->prepare("INSERT INTO comments(object_id, author, comment_text, updated_at, created_at) 
            VALUES (:objectId, :author, :commentText, current_timestamp(), current_timestamp())");
        $stmt->bindValue(':objectId', $comment->getOid());
        $stmt->bindValue(':author', $comment->getName());
        $stmt->bindValue(':commentText', $comment->getComment());
        $stmt->execute();

        return $this->conn->lastInsertId();
    }

    public function fetchCommentsForObject(string $oid)
    {
        $stmt = $this->conn->prepare("SELECT object_id as oid, author as name, comment_text as comment FROM comments WHERE object_id = :objectId");
        $stmt->bindParam(":objectId", $oid);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }
}