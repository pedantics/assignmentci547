<?php

use CI527\controllers\CommentController;
use CI527\models\Comment;
use Pecee\Http\Request;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;
use Pecee\SimpleRouter\SimpleRouter;

const ROUTE_PREFIX = '/Assignments/CI527';

SimpleRouter::get(ROUTE_PREFIX . '/hello/{name}', 'CI527\controllers\HelloWorldController@hello');
SimpleRouter::get(ROUTE_PREFIX . '/api.php', 'CI527\controllers\CommentController@getComments');
SimpleRouter::get(ROUTE_PREFIX . '/api.php/{oid}', 'CI527\controllers\CommentController@getCommentsForObject');
SimpleRouter::post(ROUTE_PREFIX . '/api.php', function() {
    $commentController = new CommentController();
    return $commentController->createComment(Comment::newInstance()
        ->oid($_POST['oid'])
        ->comment($_POST['comment'])
        ->author($_POST['name'])
    );
});
SimpleRouter::error(function(Request $request, \Exception $exception) {

    if($exception instanceof NotFoundHttpException && $exception->getCode() === 404) {
        http_response_code(404);
    }
});