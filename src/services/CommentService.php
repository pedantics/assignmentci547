<?php


namespace CI527\services;


use CI527\models\Comment;
use CI527\repositories\CommentRepository;

class CommentService
{
    private CommentRepository $commentRepository;

    public function __construct()
    {
        $this->commentRepository = new CommentRepository();
    }

    public function getAllComments() {
        $output = array();
        $comments = $this->commentRepository->fetchComments();
        foreach ($comments as $comment) {
            array_push($output, Comment::newInstance()
                ->oid($comment->object_id)
                ->comment($comment->comment_text)
                ->author($comment->author)
            );
        }
        return $output;
    }

    public function createComment(Comment $comment): int
    {
        return $this->commentRepository->createComment($comment);
    }

    public function getCommentsForObject(string $oid)
    {
        $comments = $this->commentRepository->fetchCommentsForObject($oid);
        return ["oid" => $oid, "comments" => $comments];
    }
}